This project's issue tracker has been disabled, if you wish to [create an issue or bug please follow these directions](/CONTRIBUTING.md#issue-tracker).

# Gemnasium Maven analyzer

Dependency Scanning for Java projects using either `maven` or `gradle` to manage dependencies.

**As of GitLab %15.0 and Gemnasium [v3.0.0](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/releases/v3.0.0),
gemnasium-maven is part of the [gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) project,
and this project is no longer in use.**

## Contributing

Contributors should now contribute to the [gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) project.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
